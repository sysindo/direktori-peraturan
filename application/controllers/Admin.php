<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class Admin extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('template');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->library('access');
		$this->load->library('MY_Composer');
		$this->load->model('db_model');

		if (!$this->access->is_login()) {
			$current_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
			header("Location: " . site_url('user/login?url=' . urlencode($current_url)));
		}
	}

	public function get_single_account($id)
	{
		$this->db->select('id_user,nama,email,access, jabatan,foto');
		$this->db->where('id_user', $id);
		$output = $this->db->get('dir_user')->row_array();
		rest_response(200, $output);
	}

	public function activityJson()
	{
		$this->db->limit(5);
		$this->db->order_by('submit_date', 'desc');
		$this->db->where('id_user', $this->session->userdata('user_id'));
		$data = $this->db->get('dir_content dc');
		echo json_encode($data->result_array());
	}

	public function getStat()
	{
		$thn = date("Y");
		$resultVisitor = $this->statVisitor($thn);
		$resultDownload = $this->statDownload($thn);
		$resultJenis = $this->statJenis();
		$resultBidang = $this->statBidang();

		$res = array(
			$resultVisitor,
			$resultDownload,
			$resultJenis,
			$resultBidang
		);

		echo json_encode($res);
	}

	public function statVisitor($tahun)
	{
		$data[1] = ['label'=> 'Jan','total'=>0];
		$data[2] = ['label'=> 'Feb','total'=>0];
		$data[3] = ['label'=> 'Mar','total'=>0];
		$data[4] = ['label'=> 'Apr','total'=>0];
		$data[5] = ['label'=> 'Mei','total'=>0];
		$data[6] = ['label'=> 'Jun','total'=>0];
		$data[7] = ['label'=> 'Jul','total'=>0];
		$data[8] = ['label'=> 'Ags','total'=>0];
		$data[9] = ['label'=> 'Sep','total'=>0];
		$data[10] = ['label'=> 'Okt','total'=>0];
		$data[11] = ['label'=> 'Nov','total'=>0];
		$data[12] = ['label'=> 'Des','total'=>0];

		$this->db->select('sum(total) total, month(date) bulan');
		$this->db->where('year(date)',date('Y'));
		$this->db->group_by('month(date)');
		$check = $this->db->get('dir_visitor')->result_array();
		
		foreach($check as $key => $value){
			$data[$value['bulan']]['total'] = $value['total'];
		}

		foreach($data as $key => $value){
			$output['labels'][] = $value['label'];
			$output['datasets'][] = $value['total'];
		}

		//rest_response(200, $output);
		return $output;
	}

	public function statDownload($tahun)
	{
		$this->load->model('data_model');

		$data = $this->data_model->getStatDownload($tahun)->result_array();
		$arrBulan = array("", "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des");
		foreach ($data as $key => $value) {
			$bulan = $arrBulan[(int)($value['bulan'])];
			$output['labels'][] = $bulan;
			$output['datasets'][] = ($value['jml'] != 0) ? $value['jml'] : '0';
		}
		return $output;
	}

	public function statJenis()
	{
		$this->load->model('data_model');

		$data = $this->data_model->getStatJenis()->result_array();
		foreach ($data as $key => $value) {
			
			$output['labels'][] = $value['label'];

			$output['datasets'][] = ($value['jml'] != 0) ? $value['jml'] : '0';
		}

		//rest_response(200, $output);
		return $output;
	}

	public function statBidang()
	{
		$this->load->model('data_model');

		$data = $this->data_model->getStatBidang()->result_array();
		foreach ($data as $key => $value) {
			
			$output['labels'][] = $value['label'];

			$output['datasets'][] = ($value['jml'] != 0) ? $value['jml'] : '0';
		}

		//rest_response(200, $output);
		return $output;
	}

	public function get_all_account()
	{
		$this->db->select('id_user,nama,email,access');
		$output['data'] = $this->db->get('dir_user')->result_array();
		rest_response(200, $output);
	}

	public function profil()
	{
		$data['page'] = 0;
		$id_user = $this->session->userdata('instansi');
		$data['profil'] = $this->db_model->get('dak_user', 'instansi', $id_user)->row_array();
		$data['instansi'] = $this->db_model->get('dak_instansi', 'ins_satkerkd', $id_user)->row_array();
		$this->template->display('admin/profil', $data);
	}

	public function dashboard()
	{
		$data['page'] = 'Dashboard';
		$this->template->display('admin/dashboard', $data);
	}

	public function berita()
	{
		$this->template->display('admin/berita');
	}

	public function kegiatan()
	{
		$this->template->display('admin/kegiatan');
	}

	public function direktori()
	{
		$data['page'] = 0;
		$this->template->display('admin/direktori', $data);
	}

	public function bahan_bacaan()
	{
		$this->template->display('admin/bahan_bacaan');
	}

	public function form_content($id = null)
	{
		$data['kategori'] = $this->db_model->get('dir_ms_kategori')->result_array();
		$data['jenis'] = $this->db_model->get('dir_ms_jenis')->result_array();
		$data['idData'] = $id;
		$data['page'] = 0;
		$this->template->display('admin/form_content', $data);
	}

	public function group()
	{
		$this->template->display('admin/group');
	}

	public function get_forum_topic()
	{
		$this->db->select('dir_forum_topic.*,dir_user.foto');
		$this->db->join('dir_user', 'dir_user.id_user = dir_forum_topic.created_by');
		$output['data'] = $this->db_model->get('dir_forum_topic')->result_array();
		rest_response(200, $output);
	}

	public function get_single_forum_reply($id_post)
	{
		$this->db->select('forum_post.*,user.nama');
		$this->db->join('dir_user', 'dir_user.id_user = dir_forum_post.created_by');
		$output = $this->db_model->get('forum_post', 'id_post', $id_post);
	}

	public function get_content()
	{
		$id = $this->session->userdata('user_id');
		$qry = array();

		$query = "select 
		c.*,
		dak_user.nama_lengkap,
		msk.cat_name,
		msj.jenis_name,
		msj.jenis_alias,
		te.tags
		from dir_content c 
		join dak_user on dak_user.user_id = c.id_user 
		left outer join dir_ms_kategori msk on msk.id_cat = c.id_cat 
		left outer join dir_ms_jenis msj on msj.id_jenis = c.jenis 
		left outer join (select id_item, GROUP_CONCAT(tag) as tags from dir_tags_entry te 
		left outer join dir_tags tg on tg.id_tag = te.id_tag 
		group by te.id_item ) te on te.id_item = c.id_content
		where c.id_user = ? ";
		$qry = array($id);
		$output['data'] = $this->db->query($query, $qry)->result_array();
		rest_response(200, $output);
	}

	public function get_member()
	{
		$query = "select 
		* from dir_user ";
		$output['data'] = $this->db->query($query)->result_array();
		rest_response(200, $output);
	}

	public function get_single_content($id)
	{
		$output = $this->db_model->get('dir_content', 'id_content', $id)->row_array();
		$output['tags'] = "";

		$this->db->join('dir_tags', 'dir_tags.id_tag = dir_tags_entry.id_tag');
		$this->db->where('id_item', $id);
		$tags = $this->db_model->get('dir_tags_entry')->result_array();
		foreach ($tags as $key => $value) {
			$output['tags'] .= $value['tag'] . ",";
		}

		if ($output['tags'] != "") {
			$output['tags'] = rtrim($output['tags'], ',');
		}

		rest_response(200, $output);
	}

	public function save_konten($id = "")
	{
		$this->load->model('db_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('numeric', '%s harus diisi berupa angka');
		$this->form_validation->set_error_delimiters('', ',');

		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		$this->form_validation->set_rules('tags', 'Keyword / Tag', 'required');

		if ($this->form_validation->run() == FALSE) {

			$a = explode(',', validation_errors());
			$response = array('status' => 400, 'message' => $a[0]);
			rest_response(400, $response);
		} else {
			$input = array(
				'judul' => $this->input->post('judul'),
				'deskripsi' => $this->input->post('deskripsi'),
				'no' => $this->input->post('nomor'),
				'tgl' => $this->input->post('tgl'),
				'id_user' => $this->session->userdata('user_id'),
				'jenis' => $this->input->post('jenis'),
				'id_cat' => $this->input->post('category')
			);

			if ($id == "") {
				$input['submit_date'] = date("Y-m-d H:i:s");
			} else {
				$input['updated_date'] = date("Y-m-d H:i:s");
			}

			$input_name = 'file';
			$upload_conf['allowed_types'] = "pdf";
			$upload_conf['upload_path'] = "./uploads/content";
			$file_data = $this->uploads($input_name, $upload_conf);
			if ($file_data['status'] == 200) {
				$input['file'] = $file_data['data']['name'];
				$input['link'] = '';
			} else {
				if (!empty($_FILES[$input_name]['name'])) {
					rest_response(400, $file_data);
				}else{
					if($_POST['link']!=''){
						$input['link'] = $_POST['link'];
						$input['file'] = null;
					}
				}
			}

			$res = $this->db_model->insert('dir_content', $input, 'id_content', $id);
			$id = ($id == "") ? $res : $id;

			$this->db_model->delete('dir_tags_entry', 'jenis = "' . $this->input->post('jenis') . '" and id_item=', $id);
			$tags = $this->input->post('tags');
			$tags = explode(',', $tags);
			foreach ($tags as $key => $value) {
				$check = $this->db_model->get('dir_tags', 'tag', $value)->row_array();
				$id_tag = "";
				if (empty($check)) {
					$id_tag = $this->db_model->insert('dir_tags', ['tag' => $value]);
				} else {
					$id_tag = $check['id_tag'];
				}

				$input_tags['jenis'] = $this->input->post('jenis');
				$input_tags['id_tag'] = $id_tag;
				$input_tags['id_item'] = $id;
				$this->db_model->insert('dir_tags_entry', $input_tags);
			}

			if(isset($input['file'])){
				if($input['file'] != ''){
					$this->pdf_add_watermark($input['file']);
					delete_file('uploads/content/'.$input['file']);
				}
			}

			rest_response(200);
		}
	}

	public function upload_foto_profil()
	{
		$id_user = $this->session->userdata('id');
		$this->load->model('db_model');
		$input_name = 'foto';
		$upload_conf['allowed_types'] = "jpg|jpeg|png";
		$upload_conf['upload_path'] = "./uploads/foto";
		$file_data = $this->uploads($input_name, $upload_conf);
		if ($file_data['status'] == 200) {
			$input['foto'] = $file_data['data']['name'];
		} else {
			if (!empty($_FILES[$input_name]['name'])) {
				rest_response(400, $file_data);
			} else {
				rest_response(400, 'xxx');
			}
		}

		$res = $this->db_model->insert('dir_user', $input, 'id_user', $id_user);
		rest_response(200);
	}

	public function save_profil()
	{
		$id_user = $this->session->userdata('id');
		$this->load->model('db_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('numeric', '%s harus diisi berupa angka');
		$this->form_validation->set_error_delimiters('', ',');

		$this->form_validation->set_rules('nama', 'Nama', 'required');

		if ($this->form_validation->run() == FALSE) {

			$a = explode(',', validation_errors());
			$response = array('status' => 400, 'message' => $a[0]);
			rest_response(400, $response);
		} else {
			$input = array(
				'nama' => $this->input->post('nama'),
				'nama_2' => $this->input->post('nama_2'),
				'alamat' => $this->input->post('alamat'),
				'jabatan' => $this->input->post('jabatan'),
				'instansi' => $this->input->post('instansi'),
				'no_hp' => $this->input->post('no_hp'),
				'dob' => $this->input->post('dob'),
				'is_showEmail' => $this->input->post('is_showEmail'),
				'is_showHP' => $this->input->post('is_showHP'),
				'is_allowMessage' => $this->input->post('is_allowMessage'),
				'is_showRealName' => $this->input->post('is_showRealName'),
			);

			$this->db_model->insert('dir_user', $input, 'id_user', $id_user);
			rest_response(200);
		}
	}

	public function publish_content($id, $value)
	{
		$input['status'] = $value;
		$this->db_model->insert('dir_content', $input, 'id_content', $id);
		rest_response(200);
	}

	public function download_file_content($file)
	{
		$check = $this->db_model->get('dir_content', 'file', $file)->row_array();
		if (!empty($check)) {
			$get_type = explode(".", $check['file']);
			// pr($get_type);
			download_file('uploads/content_watermark/' . $file, $check['judul'] . "." . $get_type[1]);
		} else {
			echo "<h3>File Tidak Ditemukan</h3>";
		}
	}

	public function download_cover_content($file)
	{
		$check = $this->db_model->get('dir_bahan_bacaan', 'cover', $file)->row_array();
		if (!empty($check)) {
			$get_type = explode(".", $check['cover']);
			download_file('uploads/bahan_bacaan/' . $file, $check['judul'] . "." . $get_type[1]);
		} else {
			echo "<h3>File Tidak Ditemukan</h3>";
		}
	}

	public function delete_content($id)
	{
		$table = "dir_content";
		$field = "id_content";
		$this->db_model->delete($table, $field, $id);
		rest_response(200);
	}

	public function uploads($field_name = 'file', $setting = array())
	{

		$config['encrypt_name'] = true;
		$config['upload_path'] = './uploads/bukti';
		$config['allowed_types'] = 'png|jpg|jpeg';

		foreach ($setting as $key => $value) {
			$config[$key] = $value;
		}

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0755);
		}

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($field_name)) {
			$a = $this->upload->display_errors('', '');
			$data = array('status' => 400, 'message' => $a);
		} else {
			if (is_array($this->upload->data('file_name'))) {
				$detail = $this->upload->data('file_name');
				$output['name'] = $detail['file_name'];
				$output['size'] = $detail['file_size'];
				$output['file_ext'] = $detail['file_ext'];
			} else {
				$output['name'] = $this->upload->data('file_name');
				$output['size'] = $this->upload->data('file_size');
				$output['file_ext'] = $this->upload->data('file_ext');
			}
			$data = array('status' => 200, 'message' => 'Upload Success', 'data' => $output);
		}

		return $data;
	}

	public function account_management()
	{
		$this->template->admin('admin/account_management');
	}

	public function save_account($id = '')
	{
		$this->load->model('db_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('numeric', '%s harus diisi berupa angka');
		$this->form_validation->set_message('valid_email', 'Email Tidak Valid');
		$this->form_validation->set_message('matches', '%s tidak sama');
		$this->form_validation->set_error_delimiters('', ',');

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		if ($id == '') {
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('ulangi_password', 'Password', 'required|matches[password]');
		} else {
			if ($_POST['password'] != '') {
				$this->form_validation->set_rules('ulangi_password', 'Password', 'required|matches[password]');
			}
		}

		if ($this->form_validation->run() == FALSE) {

			$a = explode(',', validation_errors());
			$response = array('status' => 400, 'message' => $a[0]);
			rest_response(400, $response);
		} else {

			$input = array(
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'access' => $this->input->post('access'),
				'is_active' => 1,
				'registration_date' => date("Y-m-d H:i:s")
			);

			if ($_POST['password'] != '') {
				$input['password'] = md5($this->input->post('password'));
			}

			$this->db->trans_start();
			$res = $this->db_model->insert('dir_user', $input, 'id_user', $id);
			$this->db->trans_complete();

			if ($this->db->trans_status() === TRUE) {
				$response = array('status' => 200, 'message' => 'OK', 'data' => '');
				rest_response(200, $response);
			} else {
				$response = array('status' => 400, 'message' => 'Internal server Error');
				rest_response(400, $response);
			}
		}
	}

	public function delete_account($id)
	{
		$this->db_model->delete('dir_user', 'id_user', $id);
		rest_response(200);
	}

	public function pdf_add_watermark($file){
        $this->load->library('pdf_watermark/Set_watermark');
        $pdf = new Set_watermark();
        $filepath="./uploads/content/".$file;
        $filepath = $this->downgrade_pdf_version($filepath, $file);

        $pageCount = $pdf->setSourceFile($filepath);
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($templateId);

            if ($size['width'] > $size['height']) {
                $pdf->AddPage('L', array($size['width'], $size['height']));
            } else {
                $pdf->AddPage('P', array($size['width'], $size['height']));
            }

            $pdf->useTemplate($templateId);
        }

        $pdf->Output("uploads/content_watermark/".$file,'F');
    }

    public function downgrade_pdf_version($file, $filename,$output_path='uploads/content/'){
        $fields = array('nama'=>'');
        // $filenames = "template/pdf/watermarxx.pdf";
        
        $files['file'] = array('name'=>$filename,'data'=>file_get_contents($file));

        // URL to upload to
        // $url = "http://localhost:56/test/ghostscript/index.php/welcome/test_curl";
        $url = $this->config->item('url_convert_pdf');

        // curl
        $curl = curl_init();
        $url_data = http_build_query($fields);
        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $post_data = $this->build_data_files($boundary, $fields, $files);
        

        $fp = fopen($output_path.$filename, 'wb');
        curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $post_data,
                CURLOPT_FILE => $fp,
                CURLOPT_HTTPHEADER => array(
                    //"Authorization: Bearer $TOKEN",
                    "Content-Type: multipart/form-data; boundary=" . $delimiter,
                    "Content-Length: " . strlen($post_data)
                ),
            )
        );

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        //echo "code: ${info['http_code']}";
        //print_r($info['request_header']);
        $err = curl_error($curl);
        curl_close($curl);
        fclose($fp);
        
        return $output_path.$filename;
    }

	public function build_data_files($boundary, $fields, $files){
        $data = '';
        $eol = "\r\n";
    
        $delimiter = '-------------' . $boundary;
    
        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }
    
    
        foreach ($files as $key => $value) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $key . '"; filename="' . $value['name'] . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary'.$eol
                ;
    
            $data .= $eol;
            $data .= $value['data'] . $eol;
        }
        $data .= "--" . $delimiter . "--".$eol;
    
    
        return $data;
    }
    
    public function build_data_files_multiple($boundary, $fields, $files){
        $data = '';
        $eol = "\r\n";
    
        $delimiter = '-------------' . $boundary;
    
        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }
    
    
        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary'.$eol
                ;
    
            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--".$eol;
    
    
        return $data;
    }
}

/* End of file monitoring.php */
/* Location: ./application/controllers/monitoring.php */
