<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent:: __construct();
		date_default_timezone_set("Asia/Bangkok");
		$this->load->library('PHPExcel');
		$this->load->library('template');
		$this->load->library('access');
		$this->load->model('db_model');
		$this->load->library('form_validation');

		if(!$this->access->is_login()){
			$current_url = urlencode(site_url());
			if(isset($_GET['url'])){
				$current_url = $_GET['url'];
			}
			header("Location: ".site_url('user/login?url='.$current_url));	
		}
	}
	
	public function index()
	{
		$this->template->admin('admin/dashboard');
	}
	
	public function logout()
	{
		$this->access->logout();
		redirect('main');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */