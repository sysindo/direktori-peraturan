<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {
	 
	function __construct()
	{
		parent:: __construct();
		$this->load->library('template');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->library('access');
		$this->load->model('db_model');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: GET");
	}

	function ganti_bahasa($language = "") {
		switch ($language){
			case "indonesia":
				$this->session->set_userdata('site_lang', $language);
				break;
			case "english":
				$this->session->set_userdata('site_lang', $language);
				break;
			default:
				$this->session->set_userdata('site_lang', 'indonesia');
		}
        redirect(base_url());
    }
	
	public function index()
	{
		// if($this->access->is_login()==TRUE){
		// 	redirect('page/dashboard');
		// }else{
		// 	redirect('main');
		// }
		$this->home();
	}
	
	public function home()
	{
		$this->load->model('data_model');
		$query = "select tag from dir_tags
		group by tag
		limit 60";
		$data['tags'] = $this->db->query($query)->result_array();
		$data['page'] = 'home';
		$this->template->display('page/home', $data);
	}

	public function login()
	{
		$this->template->display('login');
	}

	public function statJson()
	{
		$this->db->select('min(cat.cat_alias) as cat_alias, count(*) as jml');
		$this->db->join('dak_ms_kategori cat','cat.id_cat = dc.id_cat ');
		$this->db->group_by('dc.id_cat');
		$data = $this->db->get('dir_content dc');
		echo json_encode($data->result_array());
	}

	public function jenisJson()
	{
		$this->db->select('min(cat.jenis_name) as jenis_name, count(*) as jml');
		$this->db->join('dir_ms_jenis cat','cat.id_jenis = dc.jenis ');
		$this->db->group_by('dc.jenis');
		$data = $this->db->get('dir_content dc');
		echo json_encode($data->result_array());
	}

	public function search($key)
	{
		$key = urldecode($key);
		$this->load->model('data_model');
		$this->load->database();
		$this->config->load("pagination");
		$jumlah_data = $this->data_model->getSearch('','',$key);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'page/berita/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 9;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		//$data['berita'] = $this->artikel_model->AllBerita($config['per_page'],$from);
		$data['hasil'] = $this->data_model->getSearch($config['per_page'],$from,$key);
		$data['total_rows']= $jumlah_data;
		$data['katakunci']= $key;
		
		$this->template->display('page/search', $data);
	}

	public function berita()
	{
		$this->load->model('data_model');
		$this->load->database();
		$this->config->load("pagination");
		$jumlah_data = $this->data_model->getContentPage('','',1);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'page/berita/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 9;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		//$data['berita'] = $this->artikel_model->AllBerita($config['per_page'],$from);
		$data['berita'] = $this->data_model->getContentPage($config['per_page'],$from,1);
		$data['page'] = 'berita';
		$this->template->display('page/berita', $data);
	}

	public function dir($kategori)
	{
		$data['page'] = $kategori;
		$dir = $this->db_model->get('dir_ms_kategori', 'id_cat', $kategori)->row_array();
		$data['nama'] = $dir['cat_name'];
		$this->template->display('page/direktori',$data);
	}

	public function detail($jenis, $id)
	{
		$query="select 
		c.*,
		dak_user.nama_lengkap,
		msk.cat_name,
		msj.jenis_name,
		msj.jenis_alias,
		te.tags
		from dir_content c 
		join dak_user on dak_user.user_id = c.id_user 
		left outer join dir_ms_kategori msk on msk.id_cat = c.id_cat 
		left outer join dir_ms_jenis msj on msj.id_jenis = c.jenis 
		left outer join (select id_item, GROUP_CONCAT(tag) as tags from dir_tags_entry te 
		left outer join dir_tags tg on tg.id_tag = te.id_tag 
		group by te.id_item ) te on te.id_item = c.id_content
		where c.id_content like ?";
		$data['content'] = $this->db->query($query, array($id))->row_array();
		$data['page'] = '';
		$this->template->display('page/detail', $data);
		// pr($data);
	}

	public function get_content($jenis=0){
		$query="select 
		c.*,
		dak_user.nama_lengkap,
		msk.cat_name,
		msj.jenis_name,
		msj.jenis_alias,
		te.tags
		from dir_content c 
		join dak_user on dak_user.user_id = c.id_user 
		left outer join dir_ms_kategori msk on msk.id_cat = c.id_cat 
		left outer join dir_ms_jenis msj on msj.id_jenis = c.jenis 
		left outer join (select id_item, GROUP_CONCAT(tag) as tags from dir_tags_entry te 
		left outer join dir_tags tg on tg.id_tag = te.id_tag 
		group by te.id_item ) te on te.id_item = c.id_content";

		if($jenis != 0){
			$query=$query." where msk.id_cat = ?";	
		}
		$output['data'] = $this->db->query($query, array($jenis))->result_array();
		rest_response(200,$output);
	}

	public function save_log_dl(){
		$this->load->model('db_model');
		$this->load->library('form_validation');

		$this->form_validation->set_message('required', '%s harus diisi');
		$this->form_validation->set_message('numeric', '%s harus diisi berupa angka');
		$this->form_validation->set_error_delimiters('', ',');
		
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('pekerjaan','Pekerjaan','required');
		$this->form_validation->set_rules('keperluan','Keperluan','required');
		
		if($this->form_validation->run() == FALSE){

			$a = explode(',', validation_errors());
			$response = array('status'=>400,'message'=>$a[0]);
			rest_response(400, $response);
		}else{
			$input = array(
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'pekerjaan' => $this->input->post('pekerjaan'),
				'keperluan' => $this->input->post('keperluan'),
				'download_date' => date("Y-m-d H:i:s"),
				'id_direktori' => $this->input->post('id_file')
			);

			$res = $this->db_model->insert('dir_log_download',$input);
			rest_response(200);
		}
	}

}

/* End of file monitoring.php */
/* Location: ./application/controllers/monitoring.php */