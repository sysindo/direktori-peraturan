<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
class User extends CI_Controller {
	 
	function __construct()
	{
		parent:: __construct();
		$this->load->library('template');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('db_model');
		$this->load->library('access');
		$this->load->library('MY_Composer');
	}

	public function login(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		
		$this->form_validation->set_rules('username','Username','trim|required|strip_tags');
		$this->form_validation->set_rules('password','Password','trim|required|strip_tags');
		$this->form_validation->set_rules('token','token','callback_check_login');
		$this->form_validation->set_rules('g-recaptcha-response','captcha','callback_check_captcha');
		
		if($this->form_validation->run() == FALSE){
			$data['site_key'] = $this->config->item('recaptcha_sitekey');
			$this->template->display('login',$data);
		}else{
			header("Location: ".site_url('admin/dashboard'));	
		}
	}
	
	public function check_login()
	{
		$username = $this->input->post('username',TRUE);
		$password = $this->input->post('password',TRUE);
		
		$login = $this->access->login($username, $password);
		if($login){
			return TRUE;
		}else{
			$this->form_validation->set_message('check_login','Username Or Password is Invalid');
			return FALSE;
		}
	}

	public function check_captcha(){
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $header = array();
        $param = array(
            'secret' => $this->config->item('recaptcha_secretkey'),
            'response' => $this->input->post('g-recaptcha-response')
        );

        $data = curlPost($url,$header,$param);
        $data = json_decode($data,true);

		if($data['success']){
			return true;
		}else{
			$this->form_validation->set_message('check_captcha','Verifikasi Captcha Gagal');
			return FALSE;
		}
	}

	public function logout()
	{
		$this->access->logout();
		redirect('page');
	}

    
}