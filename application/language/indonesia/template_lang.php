<?php

$lang['berita'] = 'Berita';
$lang['berita_desc'] = 'Ikuti perkembangan berita tentang TPPO di sini';
$lang['kegiatan'] = 'Kegiatan';
$lang['bahan_bacaan'] = 'Bahan Bacaan';
$lang['statistik'] = 'Statistik';
$lang['forum'] = 'Forum Diskusi';
$lang['forum_diskusi_desc'] = 'Diskusikan permasalahan Anda dalam penanganan TPPO di sini';
$lang['jumlah_putusan'] = 'Jumlah Putusan';
$lang['klasifikasi_perkara'] = 'Klasifikasi Perkara';
$lang['sebaran_wilayah'] = 'Sebaran Wilayah';
$lang['tingkat_proses'] = 'Tingkat Proses';
$lang['tahun_putusan'] = 'Tahun Putusan';
$lang['klik_disini'] = "Klik Disini";
$lang['ganti_bahasa'] = "English";
$lang['ganti_bahasa_ke'] = "english";

$lang['home_header_1'] = "Apa yang Anda ketahui tentang Tindak Pidana Perdagangan Orang?";
$lang['home_header_2'] = 'Masukkan kata kunci, lalu tekan <span class="badge badge-success">ENTER</span> memulai pencarian';

//bahan bacaan = bb
$lang['bb_table_header'] = "Daftar Bahan Bacaan";

//best Practice = bp
$lang['bp_table_header'] = "Daftar Best Practice";

//E Learning = el
$lang['el_div_header'] = "Halaman E-Learning";
$lang['el_tppo_keterangan'] = "Anda bisa mengakses layanan E-Learning Tindak Pidana Perdagangan Orang melalui link di bawah ini. Layanan E-Learning ini merupakan hasil kerjasama antara Badan Diklat Kejaksaan RI dan International Organization for Migration";
$lang['el_cara_daftar'] = "Langkah-langkah registrasi E-Learning <ol> <li>Lakukan registrasi pada website E-Learning Badiklat Kejaksaan RI</li> <li>Pilih kategori <b>Diklat Kerjasama</b></li> <li>Pilih jenis <b>Diklat Tindak Pidana Perdagangan Orang</b></li> <li>Enroll sebagai peserta diklat</li> <li>Silakan pelajari materi dan selesaikan kuis yang tersedia di dalamnya</li> </ol>";

$lang['statistik_div_header'] = "Halaman Statistik";
$lang['statistik_sumber_data'] = "Sumber data : Direktori Putusan Mahkamah Agung";

$lang['forum_tambah_topik'] = "Tambah Topik";
$lang['forum_jawab'] = "Jawab Forum";


?>