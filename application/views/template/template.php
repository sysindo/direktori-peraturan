<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Direktori Peraturan</title>
    <link href="<?php echo base_url() ?>assets/css/styles-admin.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
    <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.27.0/feather.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo base_url() ?>js/jquery.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo base_url() ?>js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo base_url() ?>js/scripts-admin.js"></script>
    <script>
        $(document).ready(function() {
            var checkPage = '<?= $page; ?>';
            if (checkPage != '') {
                $('.nav-link[data-id=' + checkPage + ']').addClass('bg-danger');
            }
        });
    </script>
    <style>
        .logo img {
            height: auto;
            float: left;
            width: 70px;
        }

        .front {
            text-shadow: 0 1px 0 #ccc, 0 1px 0 #c9c9c9;
            float: left;
            margin-top: 10px;
            padding: 2px;
            color: #fff;
            text-align: left;
            font-size: 10px;
        }
    </style>
</head>

<body class="nav-fixed">
    <nav class="navbar navbar-marketing navbar-expand-lg bg-white navbar-light sticky-top">
        <div class="container ">
            <div class="logo pt-2">
                <img src="<?= base_url('assets/img/logo.png') ?>" width="100%">
            </div>
            <div class="front ml-2">
                <h3 class="text-dark" style="margin-bottom: -0.5rem !important;">Direktori Peraturan</h3>
                <h1 class="font-weight-bold my-0"><b>JAMPIDUM</b></h1>

            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mr-lg-5">
                    <?php if ($this->session->userdata('user_id') == '') { ?>
                        <li class="nav-item"><a class="nav-link active" href="<?= site_url('user/login') ?>">Login </a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="#" target="_blank">Statistik Dokumen </a></li>
                        <li class="nav-item"><a class="nav-link" href="http://cms-publik.kejaksaan.go.id/" target="_blank">Info Perkara </a></li>
                        <li class="nav-item"><a class="nav-link" href="https://jampidum.kejaksaan.go.id/" target="_blank">Website JAMPIDUM </a></li> -->



                    <?php } else { ?>
                        <li class="nav-item"><a class="nav-link active" href="<?= site_url('admin/dashboard') ?>">Dashboard </a></li>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('admin/direktori') ?>">Data Peraturan </a></li>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('admin/profil') ?>">Profil satker </a></li>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('user/logout') ?>">Logout </a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <div id="container">
        <main>
            <ul class="nav justify-content-center bg-dark">
                <li class="nav-item"><a class="nav-link text-white" data-id='home' href="<?php echo site_url('home'); ?>"><i class='fas fa-home'></i> </a></li>
                <?php foreach ($cat as $row) { ?>
                    <li class="nav-item"><a class="nav-link text-white" data-id='<?= $row['id_cat'] ?>' href="<?php echo site_url('page/dir/' . $row['id_cat']); ?>"><?= $row['cat_name'] ?> </a></li>
                <?php } ?>
            </ul>

            <?php echo $_content; ?>
            <div id="layoutDefault_footer">
                <footer class="footer pt-10 pb-5 mt-auto bg-dark footer-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="text-white footer-brand">Kejaksaan Agung Republik Indonesia</div>
                                <div class="mb-3">
                                    Website ini dikelola oleh <br>
                                    Jaksa Agung Muda Bidang Tindak Pidana Umum<br>
                                    Jl. Sultan Hasanuddin No.1 Kebayoran Baru
                                    Jakarta Selatan - Indonesia
                                    <br>
                                    Telpon : +62 21 722 1269
                                    <br>
                                    E-mail : humas.puspenkum@kejaksaan.go.id
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 mb-5 mb-lg-0">
                                        <div class="text-uppercase-expanded text-white text-xs mb-4">Link terkait</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="javascript:void(0);">Mahkamah Agung</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">KPK</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">PPATK</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Kepolisian RI</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 mb-5 mb-lg-0">
                                    <div class="text-uppercase-expanded text-xs text-white mb-4">Statistik Pengunjung</div>
                                    Online : <?= $visitor['online'] ?> Pengunjung <br>
                                    Bulan ini : <?= $visitor['month'] ?> Pengunjung <br>
                                    Tahun ini : <?= $visitor['year'] ?> Pengunjung
                                </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-5">
                        <div class="row align-items-center">
                            <div class="col-md-6 small">Copyright &copy; Kejaksaan Agung Republik Indonesia. 2021</div>
                            <div class="col-md-6 text-md-right small">
                                <a href="javascript:void(0);">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>


        </main>
    </div>
</body>

</html>