<style>
    .card {
        box-shadow: 0 0.46875rem 2.1875rem rgba(4, 9, 20, 0.03), 0 0.9375rem 1.40625rem rgba(4, 9, 20, 0.03), 0 0.25rem 0.53125rem rgba(4, 9, 20, 0.05), 0 0.125rem 0.1875rem rgba(4, 9, 20, 0.03);
        border-width: 0;
        transition: all .2s
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(26, 54, 126, 0.125);
        border-radius: .25rem
    }

    .card-body {
        flex: 1 1 auto;
        padding: 1.25rem
    }

    .vertical-timeline {
        width: 100%;
        position: relative;
        padding: 1.5rem 0 1rem
    }

    .vertical-timeline::before {
        content: '';
        position: absolute;
        top: 0;
        left: 67px;
        height: 100%;
        width: 4px;
        background: #e9ecef;
        border-radius: .25rem
    }

    .vertical-timeline-element {
        position: relative;
        margin: 0 0 1rem
    }

    .vertical-timeline--animate .vertical-timeline-element-icon.bounce-in {
        visibility: visible;
        animation: cd-bounce-1 .8s
    }

    .vertical-timeline-element-icon {
        position: absolute;
        top: 0;
        left: 60px
    }

    .vertical-timeline-element-icon .badge-dot-xl {
        box-shadow: 0 0 0 5px #fff
    }

    .badge-dot-xl {
        width: 18px;
        height: 18px;
        position: relative
    }

    .badge:empty {
        display: none
    }

    .badge-dot-xl::before {
        content: '';
        width: 10px;
        height: 10px;
        border-radius: .25rem;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -5px 0 0 -5px;
        background: #fff
    }

    .vertical-timeline-element-content {
        position: relative;
        margin-left: 90px;
        font-size: .8rem
    }

    .vertical-timeline-element-content .timeline-title {
        font-size: .8rem;
        text-transform: uppercase;
        margin: 0 0 .5rem;
        padding: 2px 0 0;
        font-weight: bold
    }

    .vertical-timeline-element-content .vertical-timeline-element-date {
        display: block;
        position: absolute;
        left: -99px;
        top: 0;
        padding-right: 10px;
        text-align: right;
        color: #adb5bd;
        font-size: .7619rem;
        white-space: nowrap
    }

    .vertical-timeline-element-content:after {
        content: "";
        display: table;
        clear: both
    }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>js/moment-with-locales.js"></script>
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto">
                        <h1 class="page-header-title">
                            Dashboard
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container mt-n10">
        <div class="row">
            <div class="col-xxl-4 col-xl-4 mb-4">
                <div class="card card-header-actions h-100">
                    <div class="card-header">
                        Aktivitas Terkini
                    </div>
                    <div class="card-body">
                        <div class="timeline vertical-timeline">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 mb-4">
                <div class="card card-header-actions h-100">
                    <div class="card-header">
                        Statistik berdasarkan Jenis
                    </div>
                    <div class="card-body">
                        <canvas id="statJenis" width="100" height="100"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 mb-4">
                <div class="card card-header-actions h-100">
                    <div class="card-header">
                    Statistik berdasarkan Direktorat
                    </div>
                    <div class="card-body">
                        <canvas id="statCat" width="100" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <!-- Example Charts for Dashboard Demo-->
        <div class="row">
            <div class="col-xl-6 mb-4">
                <div class="card mb-4">
                    <div class="card-header">Statistik Pengunjung</div>
                    <div class="card-body">
                        <canvas id="statVisitor" width="200" height="100"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 mb-4">
                <div class="card mb-4">
                    <div class="card-header">Statistik Download</div>
                    <div class="card-body">
                        <!-- <canvas id="downloadChart" width="100%" height="200"></canvas> -->
                        <canvas id="statDownload" width="200" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous">
</script>
<script src="<?php echo base_url() ?>js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js" crossorigin="anonymous"></script>


<script>
    $(document).ready(function() {
        load_stat();

        function load_stat() {
            $.getJSON("<?php echo site_url('admin/activityJson'); ?>/", function(json) {
                statHTML = '';
                $.each(json, function(key, value) {
                    // statHTML += value.submit_date + `  - User menambah Dakwaan ` + value.no + ` an.<b> ` + value.terdakwa + `</b><br/>`;
                    statHTML += `<div class="vertical-timeline-item vertical-timeline-element">
                                <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge badge-dot badge-dot-xl badge-warning"> </i> </span>
                                    <div class="vertical-timeline-element-content bounce-in">
                                        <p>Menambah data Baru <b class="text-danger">` + value.no + `</b><br/>
                                        <span class="font-weight-bold">` + value.judul + `</span></p> <span class="vertical-timeline-element-date">` + moment(value.submit_date).format('L') + `</span>
                                    </div>
                                </div>
                            </div>`;
                });
                $('.timeline').html(statHTML);
            });
        }
    });

    $.getJSON("<?php echo site_url('admin/getStat'); ?>", function(json) {
        const ctx1 = document.getElementById('statJenis').getContext('2d');
        const myChart1 = new Chart(ctx1, {
            type: 'doughnut',
            data: {
                labels: json[2].labels,
                datasets: [{
                    label: 'My First Dataset',
                    data: json[2].datasets,
                    backgroundColor: [
                        'rgb(139,0,0)',
                        'rgb(165,42,42)',
                        'rgb(178,34,34)',
                        'rgb(220,20,60)',
                        'rgb(255,99,71)',
                        'rgb(255,160,122)',
                        'rgb(255,69,0)'
                    ],
                    hoverOffset: 4
                }]
            }
        });

        const ctx2 = document.getElementById('statCat').getContext('2d');
        const myChart2 = new Chart(ctx2, {
            type: 'doughnut',
            data: {
                labels: json[3].labels,
                datasets: [{
                    label: 'My First Dataset',
                    data: json[3].datasets,
                    backgroundColor: [
                        'rgb(25,25.112)',
                        'rgb(0,0,128)',
                        'rgb(0,0,205)',
                        'rgb(0,0,255)',
                        'rgb(65.105.225)',
                        'rgb(138,43,226)',
                        'rgb(75,0.130)'
                    ],
                    hoverOffset: 4
                }]
            }
        });

        const ctx3 = document.getElementById('statVisitor').getContext('2d');
        const myChart3 = new Chart(ctx3, {
            type: 'bar',
            data: {
                labels: json[0].labels,
                datasets: [{
                    label: '# visitor',
                    data: json[0].datasets,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        const ctx4 = document.getElementById('statDownload').getContext('2d');
        const myChart4 = new Chart(ctx4, {
            type: 'bar',
            data: {
                labels: json[1].labels,
                datasets: [{
                    label: '# downloads',
                    data: json[1].datasets,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

    });
</script>