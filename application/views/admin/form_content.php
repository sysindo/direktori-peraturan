<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/tagsinput.css');?>">
<script src="<?php echo base_url('js/ckeditor/ckeditor.js');?>"></script>


<!-- Main page content-->
<div class="container mt-4">
<form action="" class="form-bahan-bacaan" id="form-bahan-bacaan" data-id=''>
	<div class="row">
		
		<div class="col-lg-9">
			<!-- Default Bootstrap Form Controls-->
			<div id="default">
				<div class="card mb-4">
					<div class="card-header">Form Editor Konten</div>
					<div class="card-body">
						<!-- Component Preview-->
						<div class="sbp-preview">
							<div class="sbp-preview-content">
									<div class="row form-group">
										<div class="col-lg-6">
											<label for="exampleFormControlInput1">Nomor</label>
											<input class="form-control" name="nomor" type="text" placeholder="Nomor" />	
										</div>
										<div class="col-lg-6">
											<label for="exampleFormControlInput1">Tanggal</label>
											<input class="form-control" name="tgl" type="date" />	
										</div>
										
									</div>
									
									<div class="form-group">
										<label for="exampleFormControlInput1">Judul</label>
										<input class="form-control" name="judul" type="text" placeholder="Masukkan judul di sini" />
									</div>
									
									<div class="row form-group">
										<div class="col-lg-6">
											<label for="exampleFormControlSelect1">Jenis</label>
											<select class="form-control" name="jenis" id="selectKategori">
												<?php 
													foreach($jenis as $row){
														echo '<option value="'.$row['id_jenis'].'">'.$row['jenis_name'].'</option>';
													}
												?>
											</select>
										</div>
										<div class="col-lg-6">
											<label for="exampleFormControlInput1">Tags input</label>
											<input class="form-control" name="tags" type="text" />
										</div>
									</div>
									<div class="form-group" id="formKategori">
										<label for="exampleFormControlSelect1">Kategori</label>
										<select class="form-control col-lg-6" name="category" id="selectKategori">
											<?php 
												foreach($kategori as $row){
													echo '<option value="'.$row['id_cat'].'">'.$row['cat_name'].'</option>';
												}
											?>
										</select>
									</div>
									<div class="form-group">
										<label for="exampleFormControlTextarea1">Deskripsi</label>
										<textarea class="form-control" id="deskripsi" name="deskripsi" rows="15"></textarea>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Solid Form Controls-->
		
			<!-- Bootstrap Docs Link-->
		</div>
		<div class="col-lg-3">
			<div class="nav-sticky">
				<div class="card">
					<div class="card-body">
						<button type="submit" class="btn btn-primary btn-block btn-simpan">Submit</button>
						<hr>
						<div class="form-group" id="formFile">
							<label for="exampleFormControlInput1">File</label>
							<input type="file" name='file'>
							<a href="#" target="_BLANK" class="download-file"><h5><span class="badge badge-secondary my-2 p-2">
							<i data-feather="paperclip"></i> Unduh file</span></h5></a>
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">URL Dokumen</label>
							<input class="form-control" name="link" type="text" />
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</form>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts-admin.js"></script>
<script src="<?php echo base_url('js/tagsinput.js');?>"></script>

<script>
    $(document).ready(function(){

		CKEDITOR.replace( 'deskripsi', {
			height: '400px',
			extraPlugins: 'forms'
		});

		$(document).on('change','input[name=file]',function(){
			var numb = $(this)[0].files[0].size / 1024 / 1024;
			numb = numb.toFixed(2);
			if (numb > 2) {
				alert('File anda pilih lebih besar dari 5Mb, gunakan google drive dan simpan linknya dikotak URL');
				$(this).val(null);
			}
		});

		$('.download-file').hide();
		var idData = '<?php echo $idData;?>';
		if (idData != ''){
			DataOnLoad();
		}
		
        var elt = $('input[name=tags]');
        elt.tagsinput('refresh');

        $(document).on('submit','.form-bahan-bacaan',function(e){
            e.preventDefault();
			$('.btn-simpan').prop('disabled',true);
			$('.btn-simpan').html('menyimpan data . . .');
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/save_konten/');?>/"+idData,
                data: new FormData(this),
                dataType:'json',
                processData: false,
                contentType: false,
                success: function (response){     
                    window.location.replace("<?php echo site_url('admin/direktori');?>");
                },
                error: function(xhr){                    
					$('.btn-simpan').prop('disabled',false);
					$('.btn-simpan').html('Submit');
                    alert(xhr.responseJSON.message);
                }
            });
        });

		function DataOnLoad(){
			$.ajax({
				type: "GET",
				url: "<?php echo site_url('admin/get_single_content');?>/"+idData,
				cache: false,
				success: function(json) {
					

					$('.form-bahan-bacaan').attr('data-id',json.id_bb);
					$('input[name=judul]').val(json.judul);
					$('input[name=nomor]').val(json.no);
					$('input[name=tgl]').val(json.tgl);
					$('textarea[name=deskripsi]').val(json.deskripsi);
					$('select[name=category]').val(json.id_cat);
					$('select[name=jenis]').val(json.jenis);

					if(json.file != null){
						$('.download-file').attr("href","<?php echo site_url('admin/download_file_content');?>/"+json.file);
						$('.download-file').show();
					} else{
						$('.download-file').hide();
					}

					if(json.link != null){
						$('input[name=link]').val(json.link);
					}
					
					elt.tagsinput('removeAll');
					// $('.bootstrap-tagsinput>.badge-info').remove();
					$('input[name=tags]').val(json.tags);
					$('input[name=tags]').focus();
					$('input[name=tags]').blur();
				},
				error: function(xhr){
					alert('error');
				}
			});
            
        };
    });
</script>