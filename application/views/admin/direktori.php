
<div class="container mt-4">
    <div class="card mb-4">
        <div class="card-header">Daftar peraturan
        <div class="float-right">
				<a href="<?php echo site_url('admin/form_content/')?>" class="btn btn-primary btn-sm float-rigth">
				Data baru
				</a>
			</div>
        </div>
        <div class="card-body">
            <div class="datatable">
                <table class="table table-bordered table-hover" width="100%" cellspacing="0" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor, tanggal</th>
                            <th>Judul</th>
                            <th>Dibuat oleh</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script>
	$(document).ready(function() {
		var tabel = $('#example').DataTable({
            ajax: '<?php echo site_url('admin/get_content/');?>',
            autoWidth: false,
            order : [0, 'asc'],
            "rowCallback": function( row, data, iDisplayIndex ) {
            var index = iDisplayIndex +1;
            $('td:eq(0)',row).html(index);
            return row;
            },
            "columns" : [
                {data: 'id_content', width: '5%'}, 
                {data: 'no', width: '10%',
                    render:function(data,type,col,meta){
                        return data+'<br/>'+col.tgl;
                    }
                },                  
                {data: 'deskripsi',
                    render:function(data,type,col,meta){
                        var linkEdit = "<?php echo site_url('admin/form_content')?>/"+col.id_content;
                        var html = '';
                        html += '<span class="badge badge-warning">'+col.jenis_name+'</span> <span class="badge badge-success">'+col.cat_name+'</span>';
                        html += '<h5><a href="'+linkEdit+'">'+col.judul+'</a></h5>';
                        html += 'Tags: '+col.tags+'<br/>';
                        html += '<a href="'+linkEdit+'" class="btn btn-primary btn-sm mt-3">Edit</a>';
                        html += ' <button class="btn btn-danger btn-sm mt-3 btn-delete" data-id-content="'+col.id_content+'">Delete</button>';
                        return html;
                    }
                },
                {data: 'nama_lengkap', width: '10%',
                    render:function(data,type,col,meta){
                        return data+'<br/><small class="text-muted">Date: '+col.submit_date+'</small>';
                    }
                }, 
                {data: 'id_user', width: '10%',
                    render:function(data,type,col,meta){
                        return '<h4><span class="badge badge-primary">New</span></h4>';
                    }
                }                  
            ],
            language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'Awal', 'last': 'Akhir', 'next': '&rarr;', 'previous': '&larr;' }
            },
            preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        $('body').on('click','.btn-delete',function(){
            if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                var idRow = $(this).attr('data-id-content');
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('admin/delete_content')?>/" + idRow,
                    cache: false,
                    success: function(html) {
                        alert('Data berhasil dihapus');
                        tabel.ajax.reload( null, false );	
                    },
                    error: function(xhr){
                        alert('error');
                    }
                });
            }
        });

	} );
</script>