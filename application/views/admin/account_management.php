<header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
    <div class="container">
        <div class="page-header-content pt-4">
            <div class="row align-items-center justify-content-between">
                <div class="col-auto mt-4">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="book-open"></i></div>
                        Account Management
                    </h1>
                    <div class="page-header-subtitle">An extended version of the DataTables library, customized for SB Admin Pro</div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container mt-n10">
    <div class="card mb-4">
        <div class="card-header">Account Management
        <div class="float-right">
				<a href="#" class="btn btn-primary btn-sm float-rigth akun-baru">
				Akun Baru
				</a>
			</div>
        </div>
        <div class="card-body">
            <div class="datatable">
                <table class="table table-bordered table-hover" width="100%" cellspacing="0" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Akses</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAkun" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Akun</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" class="form-akun" data-id="">
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleFormControlInput1">Nama</label>
                <input class="form-control" name="nama" type="text" />
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Email</label>
                <input class="form-control" name="email" type="email" />
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Password</label>
                <input class="form-control" name="password" type="password" />
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Ulangi Password</label>
                <input class="form-control" name="ulangi_password" type="password" />
            </div>
            <div class="form-group">
                <label for="aksesAdmin">Akses Admin</label>
                <select name="access" id="accessUser" class="form-control">
                    <option value="1">Admin</option>
                    <option value="2">Contributor</option>
                    <option value="3">Registered User</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" data-id="">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script>
	$(document).ready(function() {
    var tabel = $('#example').DataTable({
        ajax: '<?php echo site_url('admin/get_all_account');?>',
        autoWidth: false,
        order : [0, 'asc'],
        "rowCallback": function( row, data, iDisplayIndex ) {
        var index = iDisplayIndex +1;
        $('td:eq(0)',row).html(index);
        return row;
        },
        "columns" : [
            {data: 'id_user', width: '5%'},                    
            {data: 'nama', width: '10%'},
            {data: 'email', width: '10%'},
            {data: 'access', width: '10%',
                render:function(data,type,col,meta){
                    if(data == 1){
                        return 'Admin';
                    }else if(data == 2){
                        return 'Contributor';
                    }else{
                        return 'Registered User';
                    }
                }
            }, 
            {data: 'id_user', width: '10%',
                render:function(data,type,col,meta){
                    return '<button class="btn btn-primary edit-user" data-id="'+data+'">Edit</button> <button class="btn btn-danger delete-user" data-id="'+data+'">Hapus</button>';
                }
            }                  
        ],
        language: {
        search: '<span>Filter:</span> _INPUT_',
        searchPlaceholder: 'Type to filter...',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'Awal', 'last': 'Akhir', 'next': '&rarr;', 'previous': '&larr;' }
        },
        preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    $(document).on('click','.akun-baru',function(){
        $('.form-akun').trigger('reset');
        $('.form-akun').attr('data-id','')
        $('#modalAkun').modal('show');
    });

    $(document).on('submit','.form-akun',function(e){
        e.preventDefault();
        var idRow = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/save_account');?>/"+idRow,
            data: new FormData(this),
            dataType:'json',
            processData: false,
            contentType: false,
            success: function (response){                    
                $('#modalAkun').modal('hide');
                tabel.ajax.reload( null, false );	
            },
            error: function(xhr){                    
                alert(xhr.responseJSON.message);
            }
        });
    });

    $('body').on('click','.edit-user',function(){
        var idRow = $(this).attr('data-id');
        $.ajax({
			type: "GET",
			url: "<?php echo site_url('admin/get_single_account')?>/" + idRow,
			cache: false,
			success: function(html) {
                $('#modalAkun').modal('show');
                $('.form-akun').trigger('reset');
                $('.form-akun').attr('data-id',idRow);
                $('input[name=nama]').val(html.nama);
                $('input[name=email]').val(html.email);
                $('select[name=access]').val(html.access);
			},
			error: function(xhr){
				console.log('silahkan coba beberapa saat lagi');
			}
        });
    });

    $('body').on('click','.delete-user',function(e){
        e.preventDefault();
        if (confirm("Apakah Anda yakin akan menghapus akun ini?")) {
            var idRow = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                url: "<?php echo site_url('admin/delete_account')?>/" + idRow,
                cache: false,
                success: function(html) {
                    tabel.ajax.reload( null, false );
                },
                error: function(xhr){
                    console.log('silahkan coba beberapa saat lagi');
                }
            });
        }
    });

});
</script>