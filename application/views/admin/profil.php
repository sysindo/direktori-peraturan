
                    <div class="container mt-4">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-xl-8">
                                <!-- Account details card-->
                                <div class="card mb-4">
                                    <div class="card-header bg-danger text-white">Account Details</div>
                                    <div class="card-body">
                                        Unit/satuan kerja Anda : <b><?php echo $instansi['inst_nama']?></b>
                                        <hr>
                                        <form class="form-profil" id="form-profil" data-id=''>
                                            <!-- Form Group (username)-->
                                            <div class="form-group">
                                                <label class="small mb-1">Username</label>
                                                <input class="form-control" type="text" placeholder="Enter your username" value="<?php echo $profil['username']?>" />
                                            </div>
                                            <!-- Form Row-->
                                            <div class="form-row">
                                                <!-- Form Group (first name)-->
                                                <div class="form-group col-md-6">
                                                    <label class="small mb-1">Email</label>
                                                    <input class="form-control" name="nama" type="text" value="<?php echo $profil['email']?>" />
                                                </div>
                                            </div>
                                            <!-- Form Row        -->
                                            <div class="form-row">
                                                <!-- Form Group (organization name)-->
                                                <div class="form-group col-md-6">
                                                    <label class="small mb-1">Nama </label>
                                                    <input class="form-control" name="instansi" type="text" placeholder="Enter your organization name" value="<?php echo $profil['nama_lengkap']?>" />
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-success btn-ganti-pass" type="button">Ganti Password</button>
                                            <!-- Save changes button-->
                                            <hr>
                                            <button type="submit" class="btn btn-primary" type="button">Simpan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="gantiPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ganti password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <form action="#" id="form_edit_password">
                <div class="modal-body">
                    <div id="pass_msg"></div>
                    <div class="form-group">
                        <label for="email">Password Lama</label>
                        <input type="password" class="form-control" name="old_password" id="old_password"
                            placeholder="Old password" />
                    </div>
                    <div class="form-group">
                        <label for="email">Password Baru</label>
                        <input type="password" class="form-control" name="new_password" id="new_password"
                            placeholder="New password" />
                    </div>
                    <div class="form-group">
                        <label for="email">Ulangi Password Baru</label>
                        <input type="password" class="form-control" name="con_password" id="con_password"
                            placeholder="Retype password" />
                    </div>
                </div>
            </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
      </div>
    </div>
  </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts.js"></script>

<script>
    $(document).ready(function(){
        

        $(document).on('submit','.form-profil',function(e){
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/save_profil/');?>/",
                data: new FormData(this),
                dataType:'json',
                processData: false,
                contentType: false,
                success: function (response){     
                    $('#suksesModal').modal('show');
                    setTimeout(location.reload.bind(location), 1500);
                },
                error: function(xhr){                    
                    alert(xhr.responseJSON.message);
                }
            });
        });

        $('body').on('click','.btn-ganti-pass',function(){
            $('#gantiPassword').modal('show');
        });

        $('#submit').on('click', function(event) {
        event.preventDefault();
        var conf = confirm('Apakah Anda yakin mengubah password ini?');
        if (conf) {
            $.ajax({
                url: '<?php echo site_url('admin/edit_password');?>',
                type: 'POST',
                data: $('#form_edit_password').serialize(),
                dataType: 'json',
                success: function(response) {
                    if (response.status == "OK") {
                        $("#pass_msg").attr('class', response.css_class);
                        $('#pass_msg').html('Password has changed');
                    } else {
                        $("#pass_msg").attr('class', response.css_class);
                        $('#pass_msg').html(response.status + ' : ' + response.msg);
                    }
                },
                error: function(response) {
                    //console.log(response);
                    $("#pass_msg").attr('class', 'alert alert-danger');
                    $('#pass_msg').html('error');
                }
            });
        }
        return false;
    });
          
    });
</script>

