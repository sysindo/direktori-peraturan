<div width="100%" style="background: #eceff4; padding: 50px 20px; color: #514d6a;">
    <div style="max-width: 700px; margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody><tr>
                <td style="vertical-align: top;">Kejaksaan Republik Indonesia</td>
                <td style="text-align: right; vertical-align: middle;">
                    <span style="color: #a09bb9;">
                        Melayani dengan profesional dan penuh integritas
                    </span>
                </td>
            </tr>
        </tbody></table>
        <div style="padding: 40px 40px 20px 40px; background: #fff;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody><tr>
                    <td>
                        <h2><strong>Aktivasi user account</strong></h2><strong>
                        <p><strong style="background-color: transparent; font-size: 14px; color: rgb(81, 77, 106);"></strong>Hi <?php echo $data['nama'];?> !&nbsp;</p><p>Klik link dibawah ini untuk melakukan aktivasi akun anda <?php echo $link;?><p></p><p><br></p>
                    </strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div style="text-align: center; font-size: 12px; color: #a09bb9; margin-top: 20px">
        <p>
            Mailing System - Pelayanan Terpadu Satu Pintu&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br>
        </p>
    </div>
</div>