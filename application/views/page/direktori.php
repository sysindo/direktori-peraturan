<script type="text/javascript" src="<?php echo base_url(); ?>js/moment-with-locales.js"></script>

<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />

<div class="container">
<div class="page-header-content">
    <div class="row align-items-center justify-content-between">
        <div class="col-auto mt-4">
            <h1 class="page-header-title">
                Direktori Peraturan <b><?=$nama?></b>
            </h1>
            <div class="page-header-subtitle"></div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="datatable">
            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead id="thead">
                    <tr>
                        <th>No.</th>
                        <th>#</th>
                        <th>Desc</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>js/scripts-admin.js"></script>

<script>
    $(document).ready(function() {
        moment.locale('id');
        var tabel = $('#dataTable').DataTable({
            ajax: '<?php echo site_url('page/get_content/' . $page); ?>',
            autoWidth: true,
            order: [0, 'asc'],
            "rowCallback": function(row, data, iDisplayIndex) {
                var index = iDisplayIndex + 1;
                $('td:eq(0)', row).html(index);
                return row;
            },
            "columns": [{
                    data: 'id_content',
                    width: '1%'
                },
                {
                    data: 'cover',
                    width: '10%',
                    render: function(data, type, col, meta) {
                        if (data != null) {
                            return "<img class='img-fluid' width='100px' src='<?php echo base_url('uploads/content'); ?>/" + data + "'>";
                        } else {
                            return "<img class='img-fluid' width='100px' src='<?php echo base_url('assets/img/dummy-img.png'); ?>'>";
                        }

                    }
                },
                {
                    data: 'deskripsi',
                    width: '84%',
                    render: function(data, type, col, meta) {
                        var urlDetail = '<?php echo site_url('page/detail/4/') ?>' + col.id_content;
                        var html = '';
                        html += '<span class="badge badge-warning">' + col.jenis_name + '</span> <span class="badge badge-success">' + col.cat_name + '</span>';
                        html += '<h5><a href="' + urlDetail + '" data-id="' + col.id_bb + '" href="#">' + col.judul + '</a></h5>';
                        html += 'Tags: ' + col.tags + '<br/>';
                        html += 'Kategori: ' + col.cat_name + '<br/>';
                        html += 'Diuplaod ' + moment(col.submit_date).format('LL');
                        return html;
                    }
                }
            ],
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                //$("#thead").remove();
            },
            "language": {
                "decimal": "",
                "emptyTable": "Data tidak ditemukan",
                "info": "Menampilkan _START_ s.d _END_ dari _TOTAL_ data",
                "infoEmpty": "Menampilkan 0 s.d 0 dari 0 data",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Menampilkan _MENU_ data",
                "loadingRecords": "Loading...",
                "processing": "Processing...",
                "search": "Cari ",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "first": "Awal",
                    "last": "Akhir",
                    "next": "Selanjutnya",
                    "previous": "Sebelumnya"
                },
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });

    });
</script>