<script type="text/javascript" src="<?php echo base_url(); ?>js/moment-with-locales.js"></script>
<header class="page-header page-header-dark bg-img-cover overlay" style="background-image: url(&quot;<?= base_url('assets/img/banner.jpg') ?>&quot;)">
    <div class="page-header-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-12 col-lg-10 text-center p-10">
                    <h1 class="page-header-title">Kumpulan pedoman dan petunjuk teknis Pidana Umum</h1>
                    <p class="page-header-text mb-5">Cari peraturan terkait kebijakan dalam teknis penanganan perkara Pidana Umum di sini</p>
                    <form class="page-header-signup">
                        <div class="form-row justify-content-center">
                            <div class="col-lg-6 col-md-8">
                                <div class="form-group mr-0 mr-lg-2"><label class="sr-only" for="inputSearch">Masukkan kata kunci...</label>
                                    <input class="form-control form-control-solid rounded-pill cari-form" id="inputSearch" type="text" placeholder="Masukkan kata kunci pencarian">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4"><button class="btn btn-teal btn-block btn-marketing btn_cari rounded-pill" type="button">Cari</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="svg-border-rounded text-white">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor">
            <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
        </svg>
    </div>
</header>
<style>
    .stats-number {
        font-size: 34px;
        font-weight: 700;
        text-align: center;
        color: rgba(0, 0, 0, .87);
    }

    .stats-desc {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        max-width: 300px;
        text-align: center;
        font-size: 18px;
    }
</style>

<div class="mx-5">
    <div class="card border-bottom border-bottom-lg border-danger" style="margin-top:-50px;">
        <div class="card-body" style="padding:30px 20px">
            <div class="row statDakwaan">
            </div>
        </div>
    </div>
</div>
<div class="mt-3 mx-5">
    <div class="row">
        <div class="col-lg-3">
            <div class="card mb-3">
                <div class="card-header bg-danger text-white">Kategori</div>
                <div class="card-body jenisDokumen">
                </div>
            </div>
            <div class="card">
                <div class="card-header bg-danger text-white">Label/ Tags</div>
                <div class="card-body">
                    <?php foreach ($tags as $row) { ?>
                        <span class="btn badge bg-pink text-white btn_cari_tag" data-id="<?= $row['tag'] ?>">#<?= $row['tag'] ?></span>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-xl-9">
            <div class="card card-header-actions h-100">
                <div class="card-header text-danger heading-txt">
                    Direktori
                </div>
                <div class="card-body">
                    <table class="table table-striped table-sm" id="dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No, tanggal</th>
                                <th>Jenis</th>

                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>js/scripts-admin.js"></script>

<script>
    $(document).ready(function() {
        load_stat();

        function load_stat() {
            $.getJSON("<?php echo site_url('page/statJson'); ?>/", function(json) {
                statHTML = '';
                $.each(json, function(key, value) {
                    statHTML += `<div class="col-md-3 stats">
                        <div class="stats-number">` + value.jml + ` <small>dokumen</small></div>
                        <div class="stats-desc">` + value.cat_alias + `
                        </div>
                    </div>`;
                });
                $('.statDakwaan').html(statHTML);
            });
        }

        load_jenis();

        function load_jenis() {
            $.getJSON("<?php echo site_url('page/jenisJson'); ?>/", function(json) {
                statHTML = '';
                $.each(json, function(key, value) {
                    statHTML += `<span class="btn btn-block bg-primary text-white btn_cari_jenis" data-id="` + value.jenis_name + `">` + value.jenis_name + ` (` + value.jml + `)</span>`;
                });
                $('.jenisDokumen').html(statHTML);
            });
        }

        moment.locale('id');
        var tabel = $('#dataTable').DataTable({
            ajax: '<?php echo site_url('page/get_content/'); ?>',
            autoWidth: true,
            order: [0, 'asc'],
            "rowCallback": function(row, data, iDisplayIndex) {
                var index = iDisplayIndex + 1;
                $('td:eq(0)', row).html(index);
                return row;
            },
            "columns": [{
                    data: 'id_content',
                    width: '5%'
                },
                {
                    data: 'no',
                    width: '20%',
                    render: function(data, type, col, meta) {
                        return data + '<br/>' + moment(col.tgl).format('LL');
                    }
                },
                {
                    data: 'deskripsi',
                    width: '50%',
                    render: function(data, type, col, meta) {
                        var urlDetail = '<?php echo site_url('page/detail/4/') ?>' + col.id_content;
                        var html = '';
                        html += '<span class="badge badge-warning">' + col.jenis_name + '</span> <span class="badge badge-success">' + col.cat_name + '</span>';
                        html += '<h5>' + col.judul + ' <a href="' + urlDetail + '" data-id="' + col.id_bb + '" href="#">[detail]</a> </h5>';
                        html += 'Label: ' + col.tags + '<br/>';
                        html += '<small>Diupload ' + moment(col.submit_date).format('LL') + '</small>';
                        return html;
                    }
                },
                {
                    data: 'jenis_name',
                    visible: false
                },
                {
                    data: 'tags',
                    visible: false
                }
            ],
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                //$("#thead").remove();
            },
            "language": {
                "decimal": "",
                "emptyTable": "Data tidak ditemukan",
                "info": "Menampilkan _START_ s.d _END_ dari _TOTAL_ data",
                "infoEmpty": "Menampilkan 0 s.d 0 dari 0 data",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Menampilkan _MENU_ data",
                "loadingRecords": "Loading...",
                "processing": "Processing...",
                "search": "Cari ",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "first": "Awal",
                    "last": "Akhir",
                    "next": "Selanjutnya",
                    "previous": "Sebelumnya"
                },
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });

        $('.btn_cari_tag').on('click', function() {
            tabel.search('').draw();
            var key = $(this).attr('data-id');
            $('.heading-txt').html('Hasil pencarian dengan tag/ label : ' + key)
            tabel.columns(4).search(key).draw();
        });

        $('body').on('click','.btn_cari_jenis', function() {
            tabel.search('').draw();
            var key = $(this).attr('data-id');
            $('.heading-txt').html('Hasil pencarian dengan kategori : ' + key)
            tabel.columns(3).search(key).draw();
        });

        $('body').on('click','.btn_cari', function() {
            var key = $('.cari-form').val();
            $('.heading-txt').html('Hasil pencarian dengan kata kunci : ' + key)
            tabel.search(key).draw();
        });


    });
</script>
</body>

</html>