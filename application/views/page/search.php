<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />


<div class="mt-4">
	<div class="card">
		<div class="card-header text-dark">
        Ditemukan <b><?php echo $total_rows?></b> data dengan kata kunci <b>'<?php echo $katakunci?>'</b>  
        </div>
            <div class="card-body">

            

        <div class="timeline timeline-lg">
        <?php foreach($hasil as $row) {?>
            <div class="timeline-item">
                <div class="timeline-item-marker">
                    <div class="timeline-item-marker-text"><?php echo viewShortDate($row['submit_date']);?></div>
                    <div class="timeline-item-marker-indicator bg-green"></div>
                </div>
                <div class="timeline-item-content" style="padding-top: 0px !important;">
                <button class="btn btn-success btn-sm mb-2"><?php echo $row['cat_name'];?></button> <br>
                    <a href="<?php echo site_url('page/detail/'.$row['jenis'].'/'.$row['id_content']);?>"><h3><?php echo $row['judul'];?></h3></a>
                    
                    <?php echo substr(strip_tags($row['deskripsi']),0,100);?> ...
                    <br>Tags: <?php echo $row['tags'];?>
                </div>
            </div>
        <?php } ?>
            <!-- Timeline Item 1-->
            
        </div>
                
            <nav aria-label="Page navigation example">
                <?php 
                    echo $this->pagination->create_links();
                ?>
            </nav>
                
            </div>
	</div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts-admin.js"></script>