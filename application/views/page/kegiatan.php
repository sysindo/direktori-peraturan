<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />

<div class="page-header-content">
			<div class="row align-items-center justify-content-between">
				<div class="col-auto mt-4">
					<h1 class="page-header-title">
						Ikuti terus kegiatan yang berhubungan dengan TPPO di sini
					</h1>
					<div class="page-header-subtitle mb-5">The custom page header supports and styles Bootstrap breadcrumbs</div>
				</div>
			</div>
		</div>

<div class="">
    
    <div class="row">
        <?php foreach($kegiatan as $row){ ?>
            <div class="col-lg-3 mb-4">
                <!-- Knowledge base category card 1-->
                <div class="card h-100">
                    <img class="card-img-top" src="<?php echo base_url('uploads/content/').$row['cover'];?>" alt="...">
                    <div class="card-body">
                        <span class="text-primary"><?php echo viewDate($row['tanggal_kegiatan']);?></span>
                        <h5 class="card-title"><a class="text-black" href="<?php echo site_url('page/detail/2/'.$row['id_content']);?>"><?php echo $row['judul'];?></a></h5>
                        
                        <p class="card-text small"><?php echo substr(strip_tags($row['deskripsi']),0,100);?> (...)</p>
                        Tags: <div class="small text-muted"><?php echo $row['tags'];?></div>
                    </div>
                    <div class="card-footer"><a href="<?php echo site_url('page/detail/2/'.$row['id_content']);?>" class="btn btn-primary btn-sm">Ikuti</a></div>
                </div>
            </div>
        <?php } ?>
        
    </div>
    <nav aria-label="Page navigation example">
        <?php 
            echo $this->pagination->create_links();
        ?>
    </nav>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts-admin.js"></script>