<style>
.imgx { 
            float: left;  
            margin-right: 20px; 
            width: 50%;
        }
</style>

<div class="container">
    <!-- Knowledge base article-->
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <a class="btn btn-transparent-dark btn-icon" href="knowledge-base-category.html"><i data-feather="arrow-left"></i></a>
            <div class="ml-3">
            <h2 class="">
            <?php echo $content['judul'];?>
            </h2>
            <div class="small">Tags:  <?php echo $content['tags'];?></div>
            </div>
        </div>
        <div class="row card-body">
        <div class="col-lg-8">
        Kategori:
            <h4><?php echo $content['cat_name'];?></h4>
            Tanggal :
            <h4><?php echo viewShortDate($content['submit_date']);?></h4>
            Tags :
            <h4><?php echo $content['tags'];?></h4>
            <?php echo $content['deskripsi'];?>
            
        </div>
        <div class="col-lg-4">
                <?php if ($content['file'] != null || $content['link'] != null) { ?>
                    <div class="card">
                        <div class="card-header bg-danger text-white">
                            Form Download
                        </div>
                        <div class="card-body">
                            <form id="form-dl">
                                <input type="hidden" class="form-control" name="id_file" value="<?php echo $content['id_content']; ?>">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Nama</label>
                                    <input type="text" class="form-control" name="nama">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Email address</label>
                                    <input type="email" class="form-control" name="email" placeholder="nama@email.com">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Pekerjaan</label>
                                    <input type="text" class="form-control" placeholder="" name="pekerjaan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Keperluan</label>
                                    <select class="form-control" name="keperluan">
                                        <option>Jurnalistik</option>
                                        <option>Akademik</option>
                                        <option>Menambah wawasan</option>
                                        <option>Penelitian</option>
                                        <option>Lain-lain</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Download</button>


                            </form>
                        </div>
                    </div>
                <?php } ?>
            </div>  
        </div>
    </div>
</div>

<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Download file Peraturan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <b>Disclaimer!</b> <br>
                Jika ada perbedaan isi antara dokumen elektronik dengan dokumen fisik, maka yang dinyatakan valid adalah dokumen fisik yang telah ditandatangani dan dibacakan di dalam persidangan.
                <hr>
                <?php if ($content['file'] != null) { ?>
                <a class="btn btn-primary btn-block" href="<?= base_url('uploads/content_watermark/' . $content['file']) ?>" target="_blank">Download File</a>
                <?php }elseif($content['link'] != null){?>
                <a class="btn btn-primary btn-block" href="<?=$content['link']; ?>" target="_blank">Download File</a>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts-admin.js"></script>

<script>
    $(document).ready(function() {

        $(document).on('submit', '#form-dl', function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('page/save_log_dl/'); ?>/",
                data: new FormData(this),
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#downloadModal').modal('show');
                },
                error: function(xhr) {
                    alert(xhr.responseJSON.message);
                }
            });
        });
    });
</script>