
<div class="container">
	<div class="row my-5">
		<div class="col-lg-3">

		</div>
		<div class="col-lg-6">
		<div class="card">
		<div class="card-body p-0">
			<div class="row no-gutters">
				<!-- <div class="col-lg-6 align-self-stretch bg-img-cover d-none d-lg-flex" style='background-image: url("https://source.unsplash.com/npxXWgQ33ZQ/1200x800")'></div> -->
				<div class="col-lg-12 p-5 col-lg-offset-2 text-center">
					<h3>Login aplikasi
						<b>Direktori Peraturan</b>
					</h3>
					<form action="" target="" method="post">
					<p><?php echo validation_errors();?></p>
						<input class="form-control mb-2" name="username" type="text" placeholder="Masukkan Email">
						<input class="form-control mb-2" name="password" type="password" placeholder="Masukkan Password">
						<div class="g-recaptcha" data-sitekey="<?=$site_key?>"></div>
						<button type="submit" class="btn btn-block mt-5 btn-primary">Login</button>
					</form>
				</div>
				<!-- <div class="col-lg-6">
					<img src="<?=base_url('assets/img/login-banner.jpg')?>" width="80%" alt="">
				</div> -->
			</div>
		</div>
	</div>
		</div>
	</div>
	
</div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url()?>js/scripts-admin.js"></script>