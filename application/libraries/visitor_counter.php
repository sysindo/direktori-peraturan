<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//

class Visitor_counter{
    
    protected $_ci;
    
    function __construct()
    {
        $this->table = 'dir_visitor';

        $this->_ci =&get_instance();
        $this->_ci->load->model('db_model');
        $this->online_visitor();
    }
    
    function online_visitor()
    {       
        //jangan lupa buat file visitor_online.json difolder dibawah
        $file = "./uploads/data/visitor_online.json";
        if(is_writable($file)) {
            $data = file_get_contents($file);
            if($data != ''){
                $data = json_decode($data,true);
            }else{
                $data = ['online'=>[],'date'=>""];
            }

            if($data['date'] != date('Y-m-d')){
                $data['online'] = array();
                $data['date'] = date('Y-m-d');
            }

            $id_visitor = "";
            $session = $this->_ci->session->all_userdata();
            if(!isset($session['visitor_id'])){
                $id_visitor = md5(uniqid(mt_rand()));
                $this->_ci->session->set_userdata('visitor_id',$id_visitor);   

				$check = $this->_ci->db_model->get($this->table,'date',date('Y-m-d'))->row_array();
				if(!empty($check)){
					$input['total'] = $check['total'] + 1;
				}else{
					$input['total'] = 1;
				}
				$input['date'] = date('Y-m-d');
				$this->_ci->db_model->insert($this->table,$input,'date',date('Y-m-d'),true);

            }else{
                $id_visitor = $this->_ci->session->userdata('visitor_id');
            }
			
            $time = time() + 120;
            $data['online'][$id_visitor] = $time;
            foreach($data['online'] as $key => $value){
                if(time() > $value){
                    unset($data['online'][$key]);
                }
            }

            $fp = fopen($file, 'w');
            fwrite($fp, json_encode($data));
            fclose($fp);

			//output jumlah pengunjung online
            return count($data['online']);
        }else{
            return "file tidak ditemukan";
        }
    }

    function get_stat(){
        $this->_ci->db->select('YEAR(date) as tahun, sum(total)as total');
		$this->_ci->db->where('year(date) = year(CURDATE())');
		$this->_ci->db->group_by('YEAR(date)');
		$check = $this->_ci->db_model->get($this->table)->row_array();
        if(!empty($check)){
		    $output['year'] = $check['total'];
        }else{
            $output['year'] = 1;
        }

		$this->_ci->db->select('month(date) as bulan, sum(total)as total');
		$this->_ci->db->where('month(date) = month(CURDATE())');
		$this->_ci->db->group_by('month(date)');
		$check = $this->_ci->db_model->get($this->table)->row_array();
        if(!empty($check)){
            $output['month'] = $check['total'];
        }else{
            $output['month'] = 1;
        }
		$output['online'] = $this->online_visitor();

        return $output;
    }

}

/* End of file template.php */
/* Location: ./application/libraries/template.php */