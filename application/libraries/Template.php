<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//

class Template{
	
	protected $_ci;
	
	function __construct()
	{
		$this->_ci =&get_instance();
	}
	
	function display($template,$data=null)
	{		
		$this->_ci->load->model('db_model');
		$data['cat'] = $this->_ci->db_model->get('dir_ms_kategori')->result_array();
		$data['jenis'] = $this->_ci->db_model->get('dir_ms_jenis')->result_array();

		$data['cekhal'] = $template;
		//$data['_include']=$this->_ci->load->view('template/include',$data,TRUE);
		//$data['_header']=$this->_ci->load->view('template/header',$data,TRUE);
		$data['_content']=$this->_ci->load->view(''.$template,$data,TRUE);
		//$data['_modals']=$this->_ci->load->view('modals',$data,TRUE);
		//$data['_footer']=$this->_ci->load->view('template/footer',$data,TRUE);
		$data['visitor'] = $this->_ci->visitor_counter->get_stat();
		$this->_ci->load->view('template/template.php',$data);
	}

	function admin($template,$data=null)
	{		
		$data['cekhal'] = $template;
		//$data['_include']=$this->_ci->load->view('template/include',$data,TRUE);
		//$data['_header']=$this->_ci->load->view('template/header',$data,TRUE);
		$data['_content']=$this->_ci->load->view(''.$template,$data,TRUE);
		//$data['_modals']=$this->_ci->load->view('modals',$data,TRUE);
		//$data['_footer']=$this->_ci->load->view('template/footer',$data,TRUE);
		
		$this->_ci->load->view('template/template-admin.php',$data);
	}

}

/* End of file template.php */
/* Location: ./application/libraries/template.php */