<?php
require_once(APPPATH.'libraries/pdf_watermark/PDF_Rotate.php');
class Set_watermark extends PDF_Rotate
{
    function Header()
    {
        //Put the watermark
        $this->SetFont('Arial','B',50);
        $this->SetTextColor(255,192,203);
        $this->RotatedText(35,190,'JAMPIDUM',45);
    }

    function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }
}

