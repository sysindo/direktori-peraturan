<?php defined('BASEPATH') or exit('No direct script access allowed');

class Data_model extends CI_Model
{
	public function getContentPage($number, $offset, $jenis)
	{
		$this->db->select('c.*,
		dir_user.nama,
		msk.cat_name,
		msj.jenis_name,
		msj.jenis_alias,
		GROUP_CONCAT(tag) as tags');
		$this->db->join('dir_user', 'dir_user.id_user = c.id_user');
		$this->db->join('dir_ms_kategori msk', 'msk.id_cat = c.id_cat', 'left outer');
		$this->db->join('dir_ms_jenis msj', 'msj.id_jenis = c.jenis', 'left outer');
		$this->db->join('dir_tags_entry te', 'te.id_item = c.id_content', 'left outer');
		$this->db->join('dir_tags tg', 'tg.id_tag = te.id_tag', 'left outer');
		$this->db->order_by('submit_date', 'desc');
		$this->db->where('c.jenis', $jenis);
		$this->db->group_by('id_content');
		if (($number == '') && ($offset == '')) {
			return $this->db->get('dir_content c')->num_rows();
		} else {
			return $this->db->get('dir_content c', $number, $offset)->result_array();
		}
	}

	public function getSearch($number, $offset, $key)
	{
		$this->db->select('c.*,
		dir_user.nama,
		msk.cat_name,
		msj.jenis_name,
		msj.jenis_alias,
		GROUP_CONCAT(tag) as tags');
		$this->db->join('dir_user', 'dir_user.id_user = c.id_user');
		$this->db->join('dir_ms_kategori msk', 'msk.id_cat = c.id_cat', 'left outer');
		$this->db->join('dir_ms_jenis msj', 'msj.id_jenis = c.jenis', 'left outer');
		$this->db->join('dir_tags_entry te', 'te.id_item = c.id_content', 'left outer');
		$this->db->join('dir_tags tg', 'tg.id_tag = te.id_tag', 'left outer');
		$this->db->order_by('submit_date', 'desc');
		$this->db->like('c.judul', $key);
		$this->db->group_by('id_content');
		if (($number == '') && ($offset == '')) {
			return $this->db->get('dir_content c')->num_rows();
		} else {
			return $this->db->get('dir_content c', $number, $offset)->result_array();
		}
	}

	function getStatDownload($year)
	{
		$query = "
		select 
		data_bulan.bulan,
		b.jumlah as jml
		from 
		(
		select 1 as bulan
		union select 2 as bulan
		union select 3 as bulan
		union select 4 as bulan
		union select 5 as bulan
		union select 6 as bulan
		union select 7 as bulan
		union select 8 as bulan
		union select 9 as bulan
		union select 10 as bulan
		union select 11 as bulan
		union select 12 as bulan
		) data_bulan

		left join (select month(download_date) as bulan, count(*) as jumlah from dir_log_download
				where year(download_date)=?
		group by month(download_date)
				) b
		on data_bulan.bulan = b.bulan
		order by data_bulan.bulan
		";
		return $this->db->query($query, array($year));
	}

	function getStatJenis()
	{
		$query = "
		select b.jenis_name as label, count(*) as jml from dir_content a
		left join dir_ms_jenis b on b.id_jenis = a.jenis
		group by b.jenis_name
		order by count(*)
		";
		return $this->db->query($query);
	}

	function getStatBidang()
	{
		$query = "
		select b.cat_name as label, count(*) as jml from dir_content a
		left join dir_ms_kategori b on b.id_cat = a.id_cat
		group by b.cat_name
		order by count(*)
		";
		return $this->db->query($query);
	}
}
