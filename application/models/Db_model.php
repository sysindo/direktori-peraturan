<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Db_model extends CI_Model {

/////////////////////////Basic Script///////////////////////////////////

	public function get($table, $field = NULL, $id = NULL, $order = null, $as = 'asc')
	{

		if($id != NULL)
		{
			$this->db->where($field, $id);
		}

		if($order != NULL)
		{
			$this->db->order_by($order, $as);
		}

		return $this->db->get($table);

	}

	public function get_specific($table, $select, $field = null, $id = null, $order = null, $as = 'asc', $limit = null, $offset = 0)
	{

		if($id != NULL)
		{
			$this->db->where($field, $id);
		}

		if($order != NULL)
		{
			$this->db->order_by($order, $as);
		}

		if($limit != NULL)
		{
			$this->db->limit($limit, $offset);
		}

		$this->db->select($select);
		return $this->db->get($table);

	}

	public function insert($table, $input, $field = NULL, $id = NULL, $insert_if_not_exist = false)
	{
		if($id == NULL){
			$result = $this->db->insert($table,$input);
			$insert_id = $this->db->insert_id();
			return ($result) ? $insert_id : FALSE;
		} else {
			if($insert_if_not_exist == true){
				$this->db->where($field, $id);
				$check = $this->db->get($table); 
				if ( $check->num_rows() > 0 ){
					$this->db->where($field, $id);
					$result = $this->db->update($table,$input);
					return ($result) ? $id : FALSE;
				} else {
					$result = $this->db->insert($table,$input);
					$insert_id = $this->db->insert_id();
					return ($result) ? $insert_id : FALSE;
				}
			}else{
				$this->db->where($field, $id);
				$result = $this->db->update($table,$input);
				return ($result) ? $id : FALSE;
			}
		}
	}

	public function delete($table, $field, $id)
	{
		$this->db->where($field, $id);
		return $this->db->delete($table);
	}

/////////////////////////Advance Script//////////////////////////////////

	public function get_data_file($table, $field = NULL, $id = NULL)
	{
		if($id != NULL)
		{
			$this->db->where($field, $id);
		}
		$this->db->join('jenis', 'jenis.id_jenis = file.id_jenis');
		return $this->db->get($table);
	}

}